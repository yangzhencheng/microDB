using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Text;


namespace microDB
{
    class richTextBoxColorHandler
    {
        public richTextBoxColorHandler(RichTextBox rt, string[] strList, Color color)
        {
            _rt = rt;
            _strList = strList;
            _color = color;

            runShowText();
        }

        private void runShowText()
        {
            if (_strList.Length > 0)
            {
                for (int i = 0; i < _strList.Length; i++)
                {
                    _rt.Select(_rt.Text.IndexOf(_strList[i]), _strList[i].Length);
                    _rt.SelectionColor = _color;
                }
            }
        }

        private RichTextBox _rt;
        private string[] _strList;
        private Color _color;
    }
}